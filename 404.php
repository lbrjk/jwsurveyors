<?php
/**
 * Page template
 * ------------------------------ *
 * Template used for pages with default content/layout
 */
get_header();
the_post();
the_page_banner();
?>

<div class="content">
	<article class="post post--single">
		<h1 class="post__title">Page not found!</h1>
		<p class="lead">The page you're looking for has been moved or no longer exists.</p>
		<center>
			<p>Return to the homepage or use the site navigation to find what you're looking for.</p>
			<a href="<?php echo home_url(); ?>" class="button button--arrow button--border">Homepage</a>
		</center>
	</article>
</div>

<?php

get_footer();