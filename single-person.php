<?php
/**
 * Single
 * ------------------------------ *
 * Single blog post template
 */
get_header();
the_post();
the_page_banner();
?>

<div class="content">
	<article class="post--person post post--single">
		<header>
			<?php the_title('<h1 class="post__title">', '</h1>'); ?>
			<p class="lead"><?php the_field('person_title'); ?></p>
		</header>

		<img src="<?php echo get_post_img_url('profile'); ?>" alt="<?php the_title(); ?>" class="person__image" />
		<?php the_content(); ?>
	</article>
	<div class="trail">
		<a href="<?php echo home_url('team'); ?>">&larr; Back to Team</a>
	</div>
</div>

<?php get_footer(); ?>