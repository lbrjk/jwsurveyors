<?php
/**
 * Template name: Services
 * ------------------------------ *
 * A listing of all the services
 * I.e. child pages
 */
get_header();
the_post();
the_page_banner();
?>

<div class="content content--wide">
	<?php jw_child_cards(); ?>
</div>

<?php get_footer(); ?>