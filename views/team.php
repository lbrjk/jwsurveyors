<?php
/**
 * Template name: Team
 * ------------------------------ *
 * Used to display a listing of team members and their bio, info, etc.
 */
get_header();
the_post();
the_page_banner();
?>

<div class="content">
	<article class="post post--single">
		<?php the_title('<h1 class="post__title">', '</h1>'); ?>
		<?php the_content(); ?>
	</article>
</div>

<?php
	$people = new WP_Query("post_type=person");

	if($people->have_posts()) : $i =0;
?>

<div class="team">

	<?php while($people->have_posts()) : $people->the_post(); $i++; ?>
		<a href="<?php the_permalink(); ?>" class="person" itemscope itemtype="https://schema.org/Person" data-aos="fade-up" data-aos-delay="<?php echo $i * 50; ?>">
			<?php
				$image = has_post_thumbnail() ? get_post_img_url('profile') : IMG . '/team-default.jpg';
				echo '<img itemprop="image" src="' . $image . '" alt="Photo of ' . get_the_title() . '" />'
			?>
			<div class="person__info" >
				<h4 itemprop="name" class="person__name"><?php the_title(); ?></h4>
				<span itemprop="jobTitle" class="person__title"><?php the_field('person_title'); ?></span>
				<span class="button button--white button--arrow button--border">Read bio</span>
			</div>
		</a>
	<?php endwhile; ?>

</div>

<?php endif; wp_reset_query(); ?>

<?php get_footer(); ?>