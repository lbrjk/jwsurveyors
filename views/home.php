<?php
/**
 * Template name: Home
 * ------------------------------ *
 * Template used for custom,static homepage
 */
get_header();
the_post();
?>

<div class="hero hero--home">
	<img src="<?php echo get_post_img_url('hero');?>" alt="Julian Wilkins &amp; Co Chartered Surveyors" data-aos="fade-in" data-aos-delay="100" data-aos-duration="1000" />
	<div class="hero__content">
		<?php
			$content = get_the_content();
			$content = wp_strip_all_tags($content);

			echo '<h1 data-aos="fade-up" data-aos-delay="200">' . $content . '</h1>';
		?>
	</div>
	<span class="call-link">
		<span>Call us for a quote on</span>
		<span>
			<a href="tel:+441903872211"><?php echo get_company_info('tel'); ?></a> or <a href="tel:+442089126399">020 891 26399</a></span>
		</span>
</div>

<div class="content content--wide">
	<?php jw_child_cards(3, 'services'); ?>
</div>

<?php
	$banner_text = get_field('page_banner_text');
	$banner_img = get_field('page_banner_image');

	if($banner_text && $banner_img) :
 ?>
	<div class="content content--large">
		<div class="feature">
			<img src="<?php echo $banner_img['sizes']['medium'] ?>" alt="<?php echo $banner_img['alt']; ?>" class="feature__image" data-aos="fade-up" aos-data-duration="500" />
			<div class="feature__text" data-aos="fade-left" data-aos-delay="200">
				<p><?php echo $banner_text; ?></p>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php get_footer(); ?>