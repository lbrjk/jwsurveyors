//=require vendor/modernizr.js
//=require vendor/aos.js
//=require lib/helpers.js
//=require lib/panels.js

(function($) {

	// Init AOS
	AOS.init({
		once: true
	});

	$(document).ready(function() {

		// Mobile menu trigger
		$('.toggle--menu').click(function(){
			$(this).toggleClass('active');
			$('html').toggleClass('nav-open');
		});

		// Store header
		var header = $('.header');

		/*
			Add class to header on scroll
			Allows for hiding contact call to action
			on home page initially
		 */
		$(window).scroll(function() {
			var scroll = $(window).scrollTop();

			if (scroll >= 768) {
				header.addClass('header--scrolled');
			} else {
				header.removeClass('header--scrolled');
			}
		});

	});

})(jQuery);