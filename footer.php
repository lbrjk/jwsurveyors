		<?php if(!is_page('contact')) jw_page_call_to_action(); ?>

		<footer class="footer">

			<div class="footer__logos">
				<a href="http://www.alep.org.uk/"><img src="<?php print IMG; ?>/logo-alep.png" alt="ALEP" /></a>
				<a href="https://www.rics.org/uk/"><img src="<?php print IMG; ?>/logo-rics.png" alt="RICS" /></a>
			</div>

			<div class="footer__text">
				<p>Rustington Office:</p>
				<?php the_company_address(); ?>
				<p>London Office:</p>
				<a href="<?php echo get_company_gm_link('place', true); ?>" title="View on Google Maps" target="_blank">
					<p class="address">70 Upper Richmond Road, Putney, London SW15 2RP</p>
				</a>
			</div>

			<div class="footer-navigation">
				<nav>
					<?php clean_nav_menu('main-nav'); ?>
				</nav>
				<div class="legal">
					<?php the_field('footer_text', 'option'); ?>
					<p class="website-credit"><a href="https://studiofinesse.co.uk" target="_blank" rel="noreferrer">Website by Finesse</a>.</p>
				</div>
			</div>

		</footer>

	</div>

	<?php wp_footer(); ?>

	</body>
</html>