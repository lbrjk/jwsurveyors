<?php
/**
 * Index template
 * ------------------------------ *
 * Template used for default archive display
 */
get_header();
the_page_banner();
?>

<?php if(have_posts()) : ?>
	<div class="post-listing">
		<?php while(have_posts()) : the_post(); ?>
			<article class="post">
				<div class="content">
					<h2 class="post__title"><?php the_title(); ?></h2>
					<p class="post__date">Posted on <?php the_time('jS F, Y'); ?></p>
					<a href="<?php the_permalink(); ?>" class="button button--border button--arrow" title="Read: <?php the_title(); ?>">Read more</a>
				</div>
			</article>
		<?php endwhile; ?>
	</div>
	<?php lj_prev_next_pagination(); ?>
<?php else : ?>
	<p>No posts found.</p>
<?php endif; ?>

<?php get_footer(); ?>