<div class="content content--wide contact-blocks">
	<div class="block">
		<svg width="34" height="42" xmlns="http://www.w3.org/2000/svg"><g transform="translate(1 1)" stroke-width="2" stroke="#7F3F98" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><path d="M32 16c0 12.444-16 23.111-16 23.111S0 28.444 0 16C0 7.163 7.163 0 16 0s16 7.163 16 16z"/><circle cx="16" cy="16" r="5.333"/></g></svg>
		<div>
			<a href="<?php echo get_company_gm_link('place', true); ?>" title="View on Google Maps" target="_blank"><?php the_company_address(); ?></a>
			<?php echo the_company_tel_link(); ?>
		</div>
	</div>
	<div class="block">
		<svg width="34" height="42" xmlns="http://www.w3.org/2000/svg"><g transform="translate(1 1)" stroke-width="2" stroke="#7F3F98" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><path d="M32 16c0 12.444-16 23.111-16 23.111S0 28.444 0 16C0 7.163 7.163 0 16 0s16 7.163 16 16z"/><circle cx="16" cy="16" r="5.333"/></g></svg>
		<div>
			<a href="<?php echo get_company_gm_link('place', true); ?>" title="View on Google Maps" target="_blank">
				<p class="address">70 Upper Richmond Road, Putney, London SW15 2RP</p>
			</a>
			<a href="tel:+442089126399">020 8912 6399</a>
		</div>
	</div>
	<div class="block">
		<svg width="34" height="34" xmlns="http://www.w3.org/2000/svg"><g transform="translate(1 1)" stroke="#7F3F98" stroke-width="2" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><circle cx="16" cy="16" r="16"/><path d="M16 6.4V16l4.8 4.8"/></g></svg>
		Our office hours are: <br/>
		<?php
			$openingtimes = opening_hours_summary();
			foreach($openingtimes as $day => $hours) {
				if($hours !== 'Closed') echo $day . ', ' . $hours;
			}
		?>
	</div>
	<div class="block">
		<svg width="34" height="28" xmlns="http://www.w3.org/2000/svg"><g stroke="#7F3F98" stroke-width="2" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><path d="M4.2 1h25.6C31.56 1 33 2.44 33 4.2v19.2c0 1.76-1.44 3.2-3.2 3.2H4.2A3.21 3.21 0 0 1 1 23.4V4.2C1 2.44 2.44 1 4.2 1z"/><path d="M33 4.2L17 15.4 1 4.2"/></g></svg>
		<?php echo the_company_email_link(); ?>
	</div>
</div>