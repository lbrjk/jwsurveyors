<div class="content content--wide">
	<h2>Downloads</h2>
	<?php
		$files = get_field('resources');
		if($files) {
			$i = 0;
			foreach($files as $file) {
				$i++;
				$id = $file['ID'];
				$title = $file['title'];
				$url = $file['url'];
				$filesize = filesize(get_attached_file($id));
				$filesize = size_format($filesize, 2);
				$file_type = pathinfo(get_attached_file($id))['extension'];

				echo '<div class="panel panel--link" data-aos="fade-up" data-aos-delay="' . $i * 50 . '">';
				echo '<a href="' . $url . '" class="file">';
				echo '<span>';
				echo $title;
				echo ' <em>' . $file_type . ' / ' . $filesize . '</em>';
				echo '</span>';
				echo '</a>';
				echo '</div>';
			}
		}
	?>
</div>

<div class="content">
	<h2>Links</h2>
	<?php the_field('links'); ?>
</div>