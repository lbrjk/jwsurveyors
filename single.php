<?php
/**
 * Single
 * ------------------------------ *
 * Single blog post template
 */
get_header();
the_post();
the_page_banner();
?>

<div class="content">
	<article class="post post--single">
		<p class="post__date">Posted on <?php the_time('jS F, Y'); ?></p>
		<?php the_title('<h1 class="post__title">', '</h1>'); ?>
		<?php the_content(); ?>
	</article>
	<div class="trail">
		<a href="<?php echo home_url('news'); ?>">&larr; Back to News</a>
	</div>
</div>

<?php get_footer(); ?>