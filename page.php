<?php
/**
 * Page template
 * ------------------------------ *
 * Template used for pages with default content/layout
 */
get_header();
the_post();
the_page_banner();
?>

<div class="content">
	<article class="post post--single">
		<?php the_title('<h1 class="post__title">', '</h1>'); ?>
		<?php the_content(); ?>
	</article>
</div>

<?php

jw_page_faqs();
if(is_page('contact')) get_partial('pages/contact');
if(is_page('resources')) get_partial('pages/resources');
get_footer();