<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i|Poppins:400,400i,600,600i" rel="stylesheet">
		<?php wp_head(); ?>
		<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "LocalBusiness",
			"name": "Julian Wilkins & Co. Chartered Surveyors",
			"telephone": "01903 872211",
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "132 The Street",
				"addressLocality": "Rustington",
				"postalCode": "BN16 3DA",
				"addressCountry": "GB"
			},
			"geo": {
				"@type": "GeoCoordinates",
				"latitude": 50.8105029,
				"longitude": -0.5082437
			},
			"openingHoursSpecification": [{
				"@type": "OpeningHoursSpecification",
				"dayOfWeek": "Monday",
				"opens": "09:00",
				"closes": "17:30"
			},{
				"@type": "OpeningHoursSpecification",
				"dayOfWeek": "Tuesday",
				"opens": "09:00",
				"closes": "17:30"
			},{
				"@type": "OpeningHoursSpecification",
				"dayOfWeek": "Wednesday",
				"opens": "09:00",
				"closes": "17:30"
			},{
				"@type": "OpeningHoursSpecification",
				"dayOfWeek": "Thursday",
				"opens": "09:00",
				"closes": "17:30"
			},{
				"@type": "OpeningHoursSpecification",
				"dayOfWeek": "Friday",
				"opens": "09:00",
				"closes": "17:30"
			}]
		}
		</script>
	</head>
	<body <?php body_class(); ?>>

		<header class="header">

			<div class="header__brand">
				<a href="<?php echo home_url(); ?>">
					<img src="<?php print IMG; ?>/jw-logo-mobile.svg" alt="<?php echo get_company_info('name'); ?>" class="mobile-logo" />
					<img src="<?php print IMG; ?>/jw-logo.svg" alt="<?php echo get_company_info('name'); ?>" class="desktop-logo" />
				</a>
			</div>

			<?php if(!is_page('contact')) : ?>
				<?php include(locate_template( 'inc/header-contact-details.php')); ?>
			<?php endif; ?>

			<div class="toggle toggle--menu">
				<span class="bar"></span>
				<span class="bar"></span>
				<span class="bar"></span>
			</div>

		</header>

		<div class="site">

			<nav class="main-navigation">
				<?php clean_nav_menu('main-nav'); ?>
				<?php include(locate_template( 'inc/header-contact-details.php')); ?>
			</nav>