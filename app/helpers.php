<?php

/**
 * Include file from /inc directory
 * @param  str $path File name/path
 * @return str       Full path to included file
 */
function get_partial($path) {
	$theme_root = get_stylesheet_directory();
	$file = $theme_root . '/inc/' . $path . '.php';

	include $file;
}

function jw_validation_message($validation_result) {
	$form = $validation_result['form'];
	// Loop through each field
	foreach($form['fields'] as $field) {
		if($field['validation_message'] === 'This field is required.') {
			$field['validation_message'] = 'Required';
		}
	}

	$validation_result['form'] = $form;

	return $validation_result;
}
add_filter('gform_validation', 'jw_validation_message');