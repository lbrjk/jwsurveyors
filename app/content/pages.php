<?php

function get_img_obj($id = false) {
	global $post;

	$img = [];
	$img_id = $id ? $id : get_post_thumbnail_id($post->ID);

	$args = array(
		'post_type' => 'attachment',
		'include' => $img_id
	);

	$imgs = get_posts($args);

	if($imgs) {
		$img['title'] = $imgs[0]->post_title;
		$img['description'] = $imgs[0]->post_content;
		$img['caption'] = $imgs[0]->post_excerpt;
		$img['alt'] = get_post_meta($img_id, '_wp_attachment_image_alt', true);
		$img['sizes'] = array(
			'full' => wp_get_attachment_image_src($img_id, 'full', false)[0]
		);
		// add additional image sizes
		foreach (get_intermediate_image_sizes() as $size) {
			$img['sizes'][$size] = wp_get_attachment_image_src($img_id, $size, false)[0];
		}
	}

	return $img;
}

function get_page_banner() {
	global $post;

	if(is_home() || !has_post_thumbnail()) {
		$page_for_posts = get_option('page_for_posts');
		$img_id = get_post_thumbnail_id($page_for_posts);
	} elseif(is_singular('person')) {
		$page_for_people = get_page_by_path('team');
		$img_id = get_post_thumbnail_id($page_for_people->ID);
	} else {
		$img_id = get_post_thumbnail_id($post->ID);
	}

	$img = get_img_obj($img_id);

	return $img;
}

function the_page_banner() {
	$img = get_page_banner();

	echo '<div class="page-banner objectfit-container">';
	echo '<img src="' . $img['sizes']['hero'] . '" alt="' . $img['alt'] . '" data-aos="fade-in" data-aos-delay="100" data-aos-duration="1000" />';
	echo '</div>';
}

/**
 * Output a list of child pages in card format
 * @param  int $count The amount of pages to show
 * @param  string $page  Path of page, defaults to current page
 * @return mix        html of child cards
 */
function jw_child_cards($count = '', $page = 'current') {
	global $post;
	$slug = $page === 'current' ? $post->post_name : $page;
	$page = get_page_by_path($slug);

	echo '<div class="content__header">';
	echo '<h2 class="post__title">' . get_the_title($page->ID) . '</h2>';
	echo '<p class="lead">' . get_field('page_summary', $page->ID) . '</p>';
	echo '</div>';

	$pages = get_pages(array(
		'parent'       => $page->ID,
		'hierarchical' => false,
		'number'       => $count,
		'sort_column'  => 'menu_order',
		'sort_order'   => 'DESC'
	));

	if($pages) {
		$i = 0;
		echo '<div class="cards cards--links">';
		foreach($pages as $page) {
			$i++;
			echo '<a href="' . get_permalink($page->ID) . '" class="card" data-aos="fade-up" data-aos-delay="' . $i * 50 . '">';
			echo '<h5 class="card__title">' . get_the_title($page->ID) .  '</h5>';
			echo '<p>' . get_field('page_summary', $page->ID) . '</p>';
			echo '</a>';
		}
		echo '</div>';
	}

	if($count != '') {
		echo '<div class="content__action">';
		echo '<a href="' . home_url($slug) . '" class="button button--arrow button--border">View all ' . $slug . '</a>';
		echo '</div>';
	}
}

/**
 * Out the page call to action
 * Look for page-by-page overrides or display default options
 * @return [type] [description]
 */
function jw_page_call_to_action() {
	// Override
	$page = get_field('page_cta');
	// Default settings
	$default = get_field('default_cta', 'option');

	// Conditionally set each element
	$title = $page['title'] ? $page['title'] : $default['title'];
	$text = $page['text'] ? $page['text'] : $default['text'];
	$link = $page['link'] ? $page['link'] : $default['link'];

	echo '<div class="page-cta content">';
	echo '<h2>' . $title . '</h2>';
	echo $text;
	echo '<a href="' . $link['url'] . '" class="button button--border button--arrow">' . $link['title'] . '</a>';
	echo '</div>';
}