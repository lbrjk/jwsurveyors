<?php

function site_content() {
	$args = array(
		'labels'             => post_type_labels('Person', 'People'),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => false,
		'show_in_admin_bar'  => false,
		'query_var'          => true,
		'rewrite'            => array('slug' => 'team', 'with_front' => false),
		'capability_type'    => 'post',
		'has_archive'        => 'people',
		'hierarchical'       => false,
		'menu_position'      => 20,
		'menu_icon'			 => 'dashicons-groups',
		'supports'           => array('title', 'editor', 'thumbnail', 'page-attributes')
	);
	register_post_type('person', $args);
}
add_action('init', 'site_content');

function person_query_vars($query) {
	// if(is_admin() && in_array('person', (array) $query->get('post_type'))) {
	if($query->is_post_type_archive('person')) {
		$query->set('posts_per_page', -1);
		$query->set('orderby', 'menu_order');
		$query->set('order', 'ASC');
	}

	return $query;

}
add_action('pre_get_posts', 'person_query_vars');

/**
* Add order column to admin listing screen for team members
*/
function add_new_person_column($person_columns) {
	$person_columns['menu_order'] = "Order";
	return $person_columns;
}
add_action('manage_edit-person_columns', 'add_new_person_column');

/**
* Show custom order column values
*/
function show_order_column($name){
	global $post;

	switch ($name) {
		case 'menu_order':
		$order = $post->menu_order;
		echo $order;
		break;
	default:
		break;
	}
}
add_action('manage_person_posts_custom_column','show_order_column');

/**
* Make order column sortable
*/
function order_column_register_sortable($columns){
	$columns['menu_order'] = 'menu_order';
	return $columns;
}
add_filter('manage_edit-person_sortable_columns','order_column_register_sortable');