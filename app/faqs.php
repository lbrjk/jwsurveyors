<?php

function jw_page_faqs() {
	global $post;

	$faqs = get_field('faqs');

	if(!$faqs) {
		return;
	} else {
		echo '<div class="content content--wide">';
		echo '<h2>' . $post->post_title . ' FAQs</h2>';

		$i = 0;
		foreach($faqs as $faq) {
			$i++;
			echo '<div class="panel panel--collapse" data-aos="fade-up" data-aos-delay="' . $i * 50 . '">';
			echo '<div class="panel__head">';
			echo '<h6>' . $faq['faq_q'] . '</h6>';
			echo '</div>';
			echo '<div class="panel__content">';
			echo $faq['faq_a'];
			echo '</div>';
			echo '</div>';
		}

		echo '</div>';
	}
}